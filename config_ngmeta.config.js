
(function () {
    'use strict';

    function configMeta(ngMetaProvider) {
        ngMetaProvider.useTitleSuffix(true);
        ngMetaProvider.setDefaultTitle(data.defaultMetaTitle || '');
        ngMetaProvider.setDefaultTitleSuffix('');
        ngMetaProvider.setDefaultTag('author', data.defaultMetaAuthor || '');

        warnIfEmpty('defaultMetaTitle');
        warnIfEmpty('defaultMetaAuthor');
    }
    
    function warnIfEmpty(key) {
        if (data[key] == null || data[key] === '') {
            log('Please define `' + key + '` inside parameters.js');
        }
    }
    
    function log(message) {
        if (typeof console.warn === 'function') {
            console.warn(message);
            return;
        }
        console.log(message);
    }

    configMeta.$inject =['ngMetaProvider'];

    angular
        .module('bravoureAngularApp')
        .config(configMeta);

})();
