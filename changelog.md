## Angular Config NgMeta / Bravoure component

This Component configs in ngmeta Service in angular.

### **Versions:**

## [1.0.1] - 2018-03-13
### Added
- Take defaults from parameters.js file and warn if missing

## [1.0.0]
### Added
- Initial Stable version

---------------------
